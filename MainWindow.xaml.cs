﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Timers;

namespace Tictacto
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Point> points;
        private Moteur_Jeu? moteur_Jeu;
        private bool en_execution=false;
        public MainWindow()
        {
            InitializeComponent();
            ViderLesChamps();
          // moteur_Jeu = new Moteur_Jeu();
        }

        /// <summary>
        /// Bouton poiur lancer le jeu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
         



            if (moteur_Jeu == null)
            {
                btnStart.Content = "Arrêter";
                btnStart.Background = new SolidColorBrush(Colors.Red);
                

                moteur_Jeu = new Moteur_Jeu();
                ecran_fin.Visibility = Visibility.Collapsed;
                ViderLesChamps();

            }
            else
            {
                moteur_Jeu = null;
                btnStart.Content = "Recommencer";
                btnStart.Background = new SolidColorBrush(Colors.WhiteSmoke);
                ViderLesChamps();

               
            }

           
        }

        private void ViderLesChamps()
        {
            foreach (var child in zoneJeux.Children)
            {

                if (child is Button button)
                {
                    button.Content = string.Empty;
                    if(button.Focusable == false){
                        button.Focusable = true;
                    }
                    else
                    {
                        button.Focusable = false;
                    }
                }
            }
            msgErreur.Visibility = Visibility.Collapsed;
            en_execution = false;
        }


        private void TourOrdinateur()
        {
            if (moteur_Jeu is not null)
            {
                int[] poso = moteur_Jeu.OrdinateurJeu();
                int x = poso[0];
                int y = poso[1];

                Position position = new Position()
                {
                    x = x,
                    y = y
                };
                string positionTag = $"{position.x},{position.y}";

                foreach (var child in zoneJeux.Children)
                {
                    if (child is Button button && button.Tag.ToString() == positionTag)
                    {
                        button.Content = moteur_Jeu.JoueurCourant;
                        button.Foreground = new SolidColorBrush(moteur_Jeu.Color);
                        break;
                    }
                }

                moteur_Jeu.UpdateBoard(position, moteur_Jeu.JoueurCourant);
                AfficherEcranFin();
            }
            //else
            //{
            //    msgErreur.Visibility = Visibility;
            //}
        }

        /// <summary>
        /// Clic sur les cases
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEspaceJeu(object sender, RoutedEventArgs e)
        {
            if(en_execution)
                return; 
            
          

           if(moteur_Jeu != null)
            {
                var space = sender as Button;
                if (space != null)
                {
                    if (!String.IsNullOrWhiteSpace(space.Content.ToString()))
                        return;

                    space.Content = moteur_Jeu.JoueurCourant;
                    space.Foreground = new SolidColorBrush(moteur_Jeu.Color);

                    var coordonnees = space.Tag.ToString().Split(',');

                    int x = 0;
                    int y = 0;

                    if (moteur_Jeu.JoueurCourant == "x")
                    {
                        x = int.Parse(coordonnees[0]);
                        y = int.Parse(coordonnees[1]);


                        Position position = new Position()
                        {
                            x = x,
                            y = y
                        };
                        en_execution = true;

                        moteur_Jeu.UpdateBoard(position, moteur_Jeu.JoueurCourant);
                        AfficherEcranFin();

                        Timer timer = new Timer(1250);
                        timer.Start();

                        timer.Elapsed += (s, args) =>
                        {
                            // Code à exécuter après 1 seconde
                            Dispatcher.Invoke(() => TourOrdinateur()); 
                            timer.Dispose();
                            en_execution = false;
                        };
                        timer.AutoReset = false; 
                        timer.Start();


                        // TourOrdinateur();
                       



                    }






                }
            }
            

            
           
        }

        private void AfficherEcranFin()
        {
            if (moteur_Jeu.JeuGagner())
            {
                ecran_fin.Visibility = Visibility.Visible;
                if (moteur_Jeu.Winner == "x")
                {
                    ecran_fin.Text = "Félicitations!!!!!";
                    ecran_fin.Background=new SolidColorBrush(Colors.Green);
                    ecran_fin.Foreground = new SolidColorBrush(Colors.White);

                }
                else if(moteur_Jeu.Winner=="o")
                {
                    ecran_fin.Text = "Perdu!!!!!";
                    ecran_fin.Foreground = new SolidColorBrush(Colors.Black);
                    ecran_fin.Background = new SolidColorBrush(Colors.Red);
                }
                else
                {
                    ecran_fin.Text = "Match nul";
                    ecran_fin.Foreground = new SolidColorBrush(Colors.Black);
                    ecran_fin.Background = new SolidColorBrush(Colors.WhiteSmoke);
                }
                moteur_Jeu = null;
                en_execution = true;
                btnStart.Content = "Rejouer";
                btnStart.Background = new SolidColorBrush(Colors.WhiteSmoke);
                
            }
            else
            {
                moteur_Jeu.JoueurSuivant();
            }
        }

    }
}
