﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tictacto
{
    public class Moteur_Jeu
    {

		private const string x = "x";
		private const string o = "o";
        private string _joueurCourrant=x;
		private Color _color = Colors.White;
		private string[,] Board = new string[3, 3];
		public string Winner;

		public Color Color
		{
			get { return _color; }
			set { _color = value; }
		}



		public string JoueurCourant
		{
			get { return _joueurCourrant; }
			set { _joueurCourrant = value; }
		}


		/// <summary>
		/// Méthode pour changer de joueur
		/// </summary>
		public void JoueurSuivant()
		{
		
			JoueurCourant =(JoueurCourant== x )? o : x;
			Color = (Color == Colors.White) ? Colors.Red : Colors.White;
			
		}


		/// <summary>
		/// Verifie si un joueur à 3 alignement
		/// </summary>
		/// <returns> true si c'est le cas</returns>
		public bool JeuGagner()
		{
            for (int i = 0; i < 3; i++)
            {
                if (!String.IsNullOrWhiteSpace(Board[0, i]))
                {
                    if (Board[0, i] == Board[1, i] && Board[2, i] == Board[0, i] && Board[2, i] == Board[1, i]) //vérifier les colonnes
                    {
                        Winner = Board[0, i];
                        return true;
                    }



                }

                if (!String.IsNullOrWhiteSpace(Board[i, 0])) //vérifier les lignes)
                {
                    if (Board[i, 0] == Board[i, 1] && Board[i, 2] == Board[i, 0] && Board[i, 2] == Board[i, 1])
                    {
                        Winner = Board[i, 0];
                        return true;
                    }
                }

                //vérifier les diagonales)

                if (!String.IsNullOrWhiteSpace(Board[1, 1]))
                {
                    if (Board[0, 0] == Board[1, 1] && Board[2, 2] == Board[1, 1] && Board[2, 2] == Board[0, 0])
                    {
                        Winner = (Board[1, 1]);
                        return true;
                    }

                    if (Board[0, 2] == Board[1, 1] && Board[2, 0] == Board[1, 1] && Board[2, 0] == Board[0, 2])
                    {
                        Winner = (Board[1, 1]);
                        return true;
                    }
                }

                if (TousLesChampsSontCharges() && Winner is null)
                {
                    //Winner prend la valeur de p, mais je retourne quand même true pour ma fenêtre de fin(ecran_fin)

                    Winner = "p";
                    return true;
                }

            }

           
		
			
			return false;
		}


		/// <summary>
		/// Permet de modifier mon tableau de position
		/// </summary>
		/// <param name="position"></param>
		/// <param name="joueurCourant"></param>
        public void UpdateBoard(Position position, string joueurCourant)
        {
			Board[position.x, position.y] = joueurCourant;
        }



		/// <summary>
		/// Génère une position de jeu pour l'ordinateur
		/// </summary>
		public int[]? OrdinateurJeu()
		{
			Random rnd = new Random();
			int xValue=0;
			int yValue= rnd.Next(3);
			int []? entrer=null ;
			//do
			//{
			//             xValue = rnd.Next(3);
			//             yValue = rnd.Next(3);
			//	i++;

			//         } 
			//while (Board[xValue, yValue] is not null && i<9);

			//bool sort = false;

			if (!TousLesChampsSontCharges())
			{
                while (entrer is null)
                {
                    xValue = rnd.Next(3); yValue = rnd.Next(3);
                    if (Board[xValue, yValue] == null)
                    {

                        entrer = new int[] { xValue, yValue };
                        return entrer;
                    }


                }
            }

		
			return null;
		}


		/// <summary>
		/// Vérifie si tous les champs sont chargés
		/// </summary>
		/// <returns></returns>
        private bool TousLesChampsSontCharges()
        {
            for (int i = 0; i < Board.GetLength(0); i++)
            {
                for (int j = 0; j < Board.GetLength(1); j++)
                {
                    if (string.IsNullOrWhiteSpace(Board[i, j]))
                    {
                        return false; // Si un champ est vide, retourne false immédiatement
                    }
                }
            }
            return true; // Si tous les champs sont chargés, retourne true
        }
    }
}
